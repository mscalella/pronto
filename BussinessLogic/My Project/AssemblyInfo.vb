﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("BusinessLogic")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("BDL Consultores")> 
<Assembly: AssemblyProduct("BusinessLogic")> 
<Assembly: AssemblyCopyright("© BDL Consultores 2008")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de la biblioteca de tipos si este proyecto se expone a COM
<Assembly: Guid("f8e5ce89-be5e-4c7a-8bf3-8c85acd75fe4")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de versión de compilación
'      Revisión
'
' Puede especificar todos los valores o establecer como predeterminados los números de versión de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.33.*")> 
'<Assembly: AssemblyFileVersion("1.0.0.0")> 

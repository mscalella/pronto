﻿CREATE TABLE [dbo].[Acopios] (
    [IdAcopio]              INT          IDENTITY (1, 1) NOT NULL,
    [NumeroAcopio]          INT          NULL,
    [IdObra]                INT          NULL,
    [IdCliente]             INT          NULL,
    [Fecha]                 DATETIME     NULL,
    [Realizo]               INT          NULL,
    [Aprobo]                INT          NULL,
    [Nombre]                VARCHAR (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Observaciones]         NTEXT        COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [IdAutorizacion1]       INT          NULL,
    [FechaAutorizacion1]    DATETIME     NULL,
    [IdAutorizacion2]       INT          NULL,
    [FechaAutorizacion2]    DATETIME     NULL,
    [IdAutorizacion3]       INT          NULL,
    [FechaAutorizacion3]    DATETIME     NULL,
    [MontoPrevisto]         NUMERIC (20) NULL,
    [FechaAprobacion]       DATETIME     NULL,
    [Estado]                VARCHAR (2)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [UsuarioAnulacion]      VARCHAR (6)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Fechaanulacion]        DATETIME     NULL,
    [MotivoAnulacion]       NTEXT        COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [IdComprador]           INT          NULL,
    [IdAutorizoCumplido]    INT          NULL,
    [IdDioPorCumplido]      INT          NULL,
    [FechaDadoPorCumplido]  DATETIME     NULL,
    [ObservacionesCumplido] NTEXT        COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [EnviarEmail]           TINYINT      NULL,
    [IdAcopioOriginal]      INT          NULL,
    [IdOrigenTransmision]   INT          NULL,
    CONSTRAINT [PK_Acopios] PRIMARY KEY CLUSTERED ([IdAcopio] ASC) WITH (FILLFACTOR = 90)
);


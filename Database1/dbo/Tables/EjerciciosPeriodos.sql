﻿CREATE TABLE [dbo].[EjerciciosPeriodos] (
    [IdEjercicioPeriodo] INT      IDENTITY (1, 1) NOT NULL,
    [Ejercicio]          INT      NULL,
    [FechaInicio]        DATETIME NULL,
    [FechaFinalizacion]  DATETIME NULL
);


﻿CREATE TABLE [dbo].[_Hawk] (
    [Cliente]                VARCHAR (50)  NULL,
    [EmailCliente]           VARCHAR (200) NULL,
    [TelefonoCliente]        VARCHAR (50)  NULL,
    [IdCliente]              INT           NULL,
    [TecnologiaEquipo]       VARCHAR (50)  NULL,
    [NumeroSerieEquipo]      VARCHAR (20)  NULL,
    [NumeroCelularSIMEquipo] NUMERIC (18)  NULL,
    [DominioVehiculo]        VARCHAR (13)  NULL,
    [MarcaVehiculo]          VARCHAR (50)  NULL,
    [ModeloVehiculo]         VARCHAR (50)  NULL,
    [ColorVehiculo]          VARCHAR (50)  NULL,
    [Estado]                 VARCHAR (5)   NULL,
    [Servicio]               VARCHAR (50)  NULL,
    [CentroInstalacion]      VARCHAR (50)  NULL,
    [Baja]                   VARCHAR (1)   NULL
);


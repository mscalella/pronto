﻿CREATE TABLE [dbo].[Remitos] (
    [IdRemito]                     INT             IDENTITY (1, 1) NOT NULL,
    [NumeroRemito]                 INT             NULL,
    [IdCliente]                    INT             NULL,
    [FechaRemito]                  DATETIME        NULL,
    [IdCondicionVenta]             INT             NULL,
    [Anulado]                      VARCHAR (2)     NULL,
    [FechaAnulacion]               DATETIME        NULL,
    [Observaciones]                NTEXT           NULL,
    [ArchivoAdjunto1]              VARCHAR (100)   NULL,
    [ArchivoAdjunto2]              VARCHAR (100)   NULL,
    [ArchivoAdjunto3]              VARCHAR (100)   NULL,
    [ArchivoAdjunto4]              VARCHAR (100)   NULL,
    [ArchivoAdjunto5]              VARCHAR (100)   NULL,
    [ArchivoAdjunto6]              VARCHAR (100)   NULL,
    [ArchivoAdjunto7]              VARCHAR (100)   NULL,
    [ArchivoAdjunto8]              VARCHAR (100)   NULL,
    [ArchivoAdjunto9]              VARCHAR (100)   NULL,
    [ArchivoAdjunto10]             VARCHAR (100)   NULL,
    [Destino]                      INT             NULL,
    [IdProveedor]                  INT             NULL,
    [IdTransportista]              INT             NULL,
    [TotalBultos]                  INT             NULL,
    [ValorDeclarado]               NUMERIC (18, 2) NULL,
    [FechaRegistracion]            DATETIME        NULL,
    [IdAutorizaAnulacion]          INT             NULL,
    [IdPuntoVenta]                 INT             NULL,
    [PuntoVenta]                   INT             NULL,
    [Patente]                      VARCHAR (25)    NULL,
    [Chofer]                       VARCHAR (50)    NULL,
    [NumeroDocumento]              VARCHAR (30)    NULL,
    [OrdenCarga]                   VARCHAR (10)    NULL,
    [OrdenCompra]                  VARCHAR (10)    NULL,
    [COT]                          VARCHAR (20)    NULL,
    [IdEquipo]                     INT             NULL,
    [IdObra]                       INT             NULL,
    [IdListaPrecios]               INT             NULL,
    [IdDetalleClienteLugarEntrega] INT             NULL,
    [HoraSalida]                   VARCHAR (10)    NULL,
    [PesoBruto]                    NUMERIC (18, 2) NULL,
    [Tara]                         NUMERIC (18, 2) NULL,
    CONSTRAINT [PK_Remitos] PRIMARY KEY CLUSTERED ([IdRemito] ASC) WITH (FILLFACTOR = 90)
);


﻿CREATE TABLE [dbo].[_TempCargaParcialComprobantes] (
    [Fecha]             DATETIME        NULL,
    [IdEntidad]         INT             NULL,
    [IdTipoComprobante] INT             NULL,
    [IdPuntoVenta]      INT             NULL,
    [NumeroComprobante] INT             NULL,
    [IdArticulo]        INT             NULL,
    [IdColor]           INT             NULL,
    [Talle]             VARCHAR (2)     NULL,
    [Cantidad]          NUMERIC (18, 2) NULL,
    [IdUsuario]         INT             NULL,
    [Orden]             INT             NULL
);


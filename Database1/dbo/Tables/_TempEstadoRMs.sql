﻿CREATE TABLE [dbo].[_TempEstadoRMs] (
    [IdDetalleRequerimiento]        INT      NULL,
    [IdDetalleRecepcion]            INT      NULL,
    [IdDetalleComprobanteProveedor] INT      NULL,
    [IdDetalleSalidaMateriales]     INT      NULL,
    [IdDetallePedido]               INT      NULL,
    [FechaDesde]                    DATETIME NULL,
    [FechaHasta]                    DATETIME NULL,
    [IdObra]                        INT      NULL,
    [IdComprobanteProveedor]        INT      NULL
);


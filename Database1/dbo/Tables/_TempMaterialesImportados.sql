﻿CREATE TABLE [dbo].[_TempMaterialesImportados] (
    [Codigo]      VARCHAR (20)  NULL,
    [Descripcion] VARCHAR (100) NULL,
    [IdRubro]     INT           NULL,
    [IdSubrubro]  INT           NULL,
    [Marca]       VARCHAR (2)   NULL
);


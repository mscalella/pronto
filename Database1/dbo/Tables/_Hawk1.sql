﻿CREATE TABLE [dbo].[_Hawk1] (
    [Cliente]           VARCHAR (50)  NULL,
    [Email]             VARCHAR (200) NULL,
    [Telefono]          VARCHAR (50)  NULL,
    [Tecnologia]        VARCHAR (50)  NULL,
    [NumeroSerie]       VARCHAR (50)  NULL,
    [NumeroCelularSIM]  NUMERIC (18)  NULL,
    [Dominio]           VARCHAR (13)  NULL,
    [Marca]             VARCHAR (50)  NULL,
    [Modelo]            VARCHAR (50)  NULL,
    [Color]             VARCHAR (50)  NULL,
    [Estado]            VARCHAR (50)  NULL,
    [Servicio]          VARCHAR (50)  NULL,
    [CentroInstalacion] VARCHAR (50)  NULL,
    [Id]                VARCHAR (20)  NULL,
    [Codigo]            VARCHAR (10)  NULL
);


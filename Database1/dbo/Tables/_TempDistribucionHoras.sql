﻿CREATE TABLE [dbo].[_TempDistribucionHoras] (
    [Legajo]     INT             NULL,
    [Obra]       VARCHAR (13)    NULL,
    [Porcentaje] NUMERIC (18, 4) NULL,
    [IdEmpleado] INT             NULL,
    [IdObra]     INT             NULL
);


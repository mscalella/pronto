﻿CREATE TABLE [dbo].[_ImportacionCobranzas_Historico] (
    [Fecha]        DATETIME        NULL,
    [Importe]      NUMERIC (18, 2) NULL,
    [Codigo]       INT             NULL,
    [Debe]         NUMERIC (18, 2) NULL,
    [Haber]        NUMERIC (18, 2) NULL,
    [FechaProceso] DATETIME        NULL
);


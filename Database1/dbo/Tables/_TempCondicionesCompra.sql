﻿CREATE TABLE [dbo].[_TempCondicionesCompra] (
    [IdAux]             INT            IDENTITY (1, 1) NOT NULL,
    [IdCondicionCompra] INT            NULL,
    [Dias]              INT            NULL,
    [Porcentaje]        NUMERIC (6, 2) NULL
);


﻿CREATE TABLE [dbo].[DefinicionesCuadrosContables] (
    [IdCuenta]        INT          NULL,
    [Descripcion]     VARCHAR (50) NULL,
    [IdCuentaIngreso] INT          NULL,
    [IdCuentaEgreso]  INT          NULL
);


﻿CREATE TABLE [dbo].[_TempRubrosContablesDatos] (
    [Mes]             INT             NULL,
    [Año]             INT             NULL,
    [IdRubroContable] INT             NULL,
    [IdObra]          INT             NULL,
    [Importe]         NUMERIC (18, 2) NULL
);


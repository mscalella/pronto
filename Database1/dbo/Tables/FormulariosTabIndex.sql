﻿CREATE TABLE [dbo].[FormulariosTabIndex] (
    [Formulario] VARCHAR (100) NULL,
    [Control]    VARCHAR (100) NULL,
    [Subindice]  INT           NULL,
    [TabIndex]   INT           NULL
);


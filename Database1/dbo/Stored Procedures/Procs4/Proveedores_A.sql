﻿CREATE Procedure [dbo].[Proveedores_A]

@IdProveedor int output,
@RazonSocial varchar(50),
@Direccion varchar(50),
@IdLocalidad smallint,
@CodigoPostal varchar(30),
@IdProvincia tinyint,
@IdPais int,
@Telefono1 varchar(50),
@Telefono2 varchar(50),
@Fax varchar(50),
@Email varchar(50),
@Cuit varchar(13),
@IdCodigoIva int,
@FechaAlta datetime,
@FechaUltimaCompra datetime,
@Excencion numeric(6,2),
@IdCondicionCompra int,
@Contacto varchar(50),
@IdActividad int,
@Nif varchar(30),
@IdEstado int,
@EstadoFecha datetime,
@EstadoUsuario varchar(20),
@AltaUsuario varchar(20),
@CodigoEmpresa varchar(20),
@Nombre1 varchar(100),
@Nombre2 varchar(100),
@NombreFantasia varchar(50),
@IGCondicion int,
@IGCertificadoAutoretencion varchar(2),
@IGCertificadoNORetencion varchar(2),
@IGFechaCaducidadExencion datetime,
@IGPorcentajeNORetencion Numeric(6,2),
@IvaAgenteRetencion varchar(2),
@IvaExencionRetencion varchar(2),
@IvaFechaCaducidadExencion datetime,
@IvaPorcentajeExencion Numeric(6,2),
@IBNumeroInscripcion varchar(20),
@IBCondicion int,
@IBFechaCaducidadExencion datetime,
@IBPorcentajeExencion Numeric(6,2),
@SSFechaCaducidadExencion datetime,
@SSPorcentajeExcencion Numeric(6,2),
@PaginaWeb varchar(50),
@Habitual varchar(2),
@Observaciones ntext,
@Saldo numeric(18,2),
@SaldoDocumentos numeric(18,2),
@CodigoProveedor int,
@IdCuenta int,
@NumeroIngresosBrutos varchar(30),
@IdMoneda int,
@LimiteCredito numeric(18,2),
@TipoProveedor int,
@Eventual varchar(2),
@IdTipoRetencionGanancia int,
@Confirmado varchar(2),
@CodigoPresto varchar(13),
@BienesOServicios varchar(1),
@IdIBCondicionPorDefecto int,
@RetenerSUSS varchar(2),
@ChequesALaOrdenDe varchar(100),
@FechaLimiteExentoGanancias datetime,
@FechaLimiteExentoIIBB datetime,
@IdImpuestoDirectoSUSS int,
@Importaciones_NumeroInscripcion varchar(20),
@Importaciones_DenominacionInscripcion varchar(10),
@EnviarEmail tinyint,
@REP_PROVEEDO_INS varchar(1),
@REP_PROVEEDO_UPD varchar(1),
@InformacionAuxiliar varchar(50),
@CoeficienteIIBBUnificado numeric(6,2),
@FechaUltimaPresentacionDocumentacion datetime,
@ObservacionesPresentacionDocumentacion ntext,
@FechaLimiteCondicionIVA datetime,
@CodigoSituacionRetencionIVA varchar(1),
@SUSSFechaCaducidadExencion datetime,
@Calificacion int,
@IdUsuarioIngreso int,
@FechaIngreso datetime,
@IdUsuarioModifico int,
@FechaModifico datetime,
@Exterior varchar(2),
@SujetoEmbargado varchar(2),
@SaldoEmbargo numeric(18,2),
@DetalleEmbargo varchar(50),
@PorcentajeIBDirecto numeric(6,2),
@FechaInicioVigenciaIBDirecto datetime,
@FechaFinVigenciaIBDirecto datetime,
@GrupoIIBB int,
@IvaFechaInicioExencion datetime,
@IdTransportista int,
@CodigoRetencionIVA varchar(10),
@IdListaPrecios int,
@RegimenEspecialConstruccionIIBB varchar(2),
@PorcentajeIBDirectoCapital numeric(6,2),
@FechaInicioVigenciaIBDirectoCapital datetime,
@FechaFinVigenciaIBDirectoCapital datetime,
@GrupoIIBBCapital int,
@IdCuentaProvision int,
@TextoAuxiliar1 varchar(100),
@TextoAuxiliar2 varchar(100),
@TextoAuxiliar3 varchar(100),
@InscriptoRegistroFiscalOperadoresGranos varchar(2),
@ArchivoAdjunto1 varchar(200),
@ArchivoAdjunto2 varchar(200),
@ArchivoAdjunto3 varchar(200),
@ArchivoAdjunto4 varchar(200),
@CancelacionInmediataDeDeuda varchar(2),
@DebitoAutomaticoPorDefecto varchar(2),
@RegistrarMovimientosEnCuentaCorriente varchar(2),
@FechaVencimientoParaEgresosProyectados varchar(2),
@SUSSFechaInicioVigencia datetime,
@OperacionesMercadoInternoEntidadVinculada varchar(2),
@IdCuentaAplicacion int,
@CodigoCategoriaIIBBAlternativo varchar(1),
@FechaInicialControlComprobantes datetime,
@ResolucionAfip3668 varchar(2)

AS 

INSERT INTO Proveedores
(
 RazonSocial,
 Direccion,
 IdLocalidad,
 CodigoPostal,
 IdProvincia,
 IdPais,
 Telefono1,
 Telefono2,
 Fax,
 Email,
 Cuit,
 IdCodigoIva,
 FechaAlta,
 FechaUltimaCompra,
 Excencion,
 IdCondicionCompra,
 Contacto,
 IdActividad,
 Nif,
 IdEstado,
 EstadoFecha,
 EstadoUsuario,
 AltaUsuario,
 CodigoEmpresa,
 Nombre1,
 Nombre2,
 NombreFantasia,
 IGCondicion,
 IGCertificadoAutoretencion,
 IGCertificadoNORetencion,
 IGFechaCaducidadExencion,
 IGPorcentajeNORetencion,
 IvaAgenteRetencion,
 IvaExencionRetencion,
 IvaFechaCaducidadExencion,
 IvaPorcentajeExencion,
 IBNumeroInscripcion,
 IBCondicion,
 IBFechaCaducidadExencion,
 IBPorcentajeExencion,
 SSFechaCaducidadExencion,
 SSPorcentajeExcencion,
 PaginaWeb,
 Habitual,
 Observaciones,
 Saldo,
 SaldoDocumentos,
 CodigoProveedor,
 IdCuenta,
 NumeroIngresosBrutos,
 IdMoneda,
 LimiteCredito,
 TipoProveedor,
 Eventual,
 IdTipoRetencionGanancia,
 Confirmado,
 CodigoPresto,
 BienesOServicios,
 IdIBCondicionPorDefecto,
 RetenerSUSS,
 ChequesALaOrdenDe,
 FechaLimiteExentoGanancias,
 FechaLimiteExentoIIBB,
 IdImpuestoDirectoSUSS,
 Importaciones_NumeroInscripcion,
 Importaciones_DenominacionInscripcion,
 EnviarEmail,
 REP_PROVEEDO_INS,
 REP_PROVEEDO_UPD,
 InformacionAuxiliar,
 CoeficienteIIBBUnificado,
 FechaUltimaPresentacionDocumentacion,
 ObservacionesPresentacionDocumentacion,
 FechaLimiteCondicionIVA,
 CodigoSituacionRetencionIVA,
 SUSSFechaCaducidadExencion,
 Calificacion,
 IdUsuarioIngreso,
 FechaIngreso,
 IdUsuarioModifico,
 FechaModifico,
 Exterior,
 SujetoEmbargado,
 SaldoEmbargo,
 DetalleEmbargo,
 PorcentajeIBDirecto,
 FechaInicioVigenciaIBDirecto,
 FechaFinVigenciaIBDirecto,
 GrupoIIBB,
 IvaFechaInicioExencion,
 IdTransportista,
 CodigoRetencionIVA,
 IdListaPrecios,
 RegimenEspecialConstruccionIIBB,
 PorcentajeIBDirectoCapital,
 FechaInicioVigenciaIBDirectoCapital,
 FechaFinVigenciaIBDirectoCapital,
 GrupoIIBBCapital,
 IdCuentaProvision,
 TextoAuxiliar1,
 TextoAuxiliar2,
 TextoAuxiliar3,
 InscriptoRegistroFiscalOperadoresGranos,
 ArchivoAdjunto1,
 ArchivoAdjunto2,
 ArchivoAdjunto3,
 ArchivoAdjunto4,
 CancelacionInmediataDeDeuda,
 DebitoAutomaticoPorDefecto,
 RegistrarMovimientosEnCuentaCorriente,
 FechaVencimientoParaEgresosProyectados,
 SUSSFechaInicioVigencia,
 OperacionesMercadoInternoEntidadVinculada,
 IdCuentaAplicacion,
 CodigoCategoriaIIBBAlternativo,
 FechaInicialControlComprobantes,
 ResolucionAfip3668
)
VALUES 
(
 @RazonSocial,
 @Direccion,
 @IdLocalidad,
 @CodigoPostal,
 @IdProvincia,
 @IdPais,
 @Telefono1,
 @Telefono2,
 @Fax,
 @Email,
 @Cuit,
 @IdCodigoIva,
 @FechaAlta,
 @FechaUltimaCompra,
 @Excencion,
 @IdCondicionCompra,
 @Contacto,
 @IdActividad,
 @Nif,
 @IdEstado,
 @EstadoFecha,
 @EstadoUsuario,
 @AltaUsuario,
 @CodigoEmpresa,
 @Nombre1,
 @Nombre2,
 @NombreFantasia,
 @IGCondicion,
 @IGCertificadoAutoretencion,
 @IGCertificadoNORetencion,
 @IGFechaCaducidadExencion,
 @IGPorcentajeNORetencion,
 @IvaAgenteRetencion,
 @IvaExencionRetencion,
 @IvaFechaCaducidadExencion,
 @IvaPorcentajeExencion,
 @IBNumeroInscripcion,
 @IBCondicion,
 @IBFechaCaducidadExencion,
 @IBPorcentajeExencion,
 @SSFechaCaducidadExencion,
 @SSPorcentajeExcencion,
 @PaginaWeb,
 @Habitual,
 @Observaciones,
 @Saldo,
 @SaldoDocumentos,
 @CodigoProveedor,
 @IdCuenta,
 @NumeroIngresosBrutos,
 @IdMoneda,
 @LimiteCredito,
 @TipoProveedor,
 @Eventual,
 @IdTipoRetencionGanancia,
 @Confirmado, 
 @CodigoPresto,
 @BienesOServicios,
 @IdIBCondicionPorDefecto,
 @RetenerSUSS,
 @ChequesALaOrdenDe,
 @FechaLimiteExentoGanancias,
 @FechaLimiteExentoIIBB,
 @IdImpuestoDirectoSUSS,
 @Importaciones_NumeroInscripcion,
 @Importaciones_DenominacionInscripcion,
 @EnviarEmail,
 @REP_PROVEEDO_INS,
 @REP_PROVEEDO_UPD,
 @InformacionAuxiliar,
 @CoeficienteIIBBUnificado,
 @FechaUltimaPresentacionDocumentacion,
 @ObservacionesPresentacionDocumentacion,
 @FechaLimiteCondicionIVA,
 @CodigoSituacionRetencionIVA,
 @SUSSFechaCaducidadExencion,
 @Calificacion,
 @IdUsuarioIngreso,
 @FechaIngreso,
 @IdUsuarioModifico,
 @FechaModifico,
 @Exterior,
 @SujetoEmbargado, 
 @SaldoEmbargo,
 @DetalleEmbargo,
 @PorcentajeIBDirecto,
 @FechaInicioVigenciaIBDirecto,
 @FechaFinVigenciaIBDirecto,
 @GrupoIIBB,
 @IvaFechaInicioExencion,
 @IdTransportista,
 @CodigoRetencionIVA,
 @IdListaPrecios,
 @RegimenEspecialConstruccionIIBB,
 @PorcentajeIBDirectoCapital,
 @FechaInicioVigenciaIBDirectoCapital,
 @FechaFinVigenciaIBDirectoCapital,
 @GrupoIIBBCapital,
 @IdCuentaProvision,
 @TextoAuxiliar1,
 @TextoAuxiliar2,
 @TextoAuxiliar3,
 @InscriptoRegistroFiscalOperadoresGranos,
 @ArchivoAdjunto1,
 @ArchivoAdjunto2,
 @ArchivoAdjunto3,
 @ArchivoAdjunto4,
 @CancelacionInmediataDeDeuda,
 @DebitoAutomaticoPorDefecto,
 @RegistrarMovimientosEnCuentaCorriente,
 @FechaVencimientoParaEgresosProyectados,
 @SUSSFechaInicioVigencia,
 @OperacionesMercadoInternoEntidadVinculada,
 @IdCuentaAplicacion,
 @CodigoCategoriaIIBBAlternativo,
 @FechaInicialControlComprobantes,
 @ResolucionAfip3668
)

SELECT @IdProveedor=@@identity

RETURN(@IdProveedor)
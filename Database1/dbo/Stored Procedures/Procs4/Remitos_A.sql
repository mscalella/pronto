﻿CREATE Procedure [dbo].[Remitos_A]

@IdRemito int  output,
@NumeroRemito int,
@IdCliente int,
@FechaRemito datetime,
@IdCondicionVenta int,
@Anulado varchar(2),
@FechaAnulacion datetime,
@Observaciones ntext,
@ArchivoAdjunto1 varchar(100),
@ArchivoAdjunto2 varchar(100),
@ArchivoAdjunto3 varchar(100),
@ArchivoAdjunto4 varchar(100),
@ArchivoAdjunto5 varchar(100),
@ArchivoAdjunto6 varchar(100),
@ArchivoAdjunto7 varchar(100),
@ArchivoAdjunto8 varchar(100),
@ArchivoAdjunto9 varchar(100),
@ArchivoAdjunto10 varchar(100),
@Destino int,
@IdProveedor int,
@IdTransportista int,
@TotalBultos int,
@ValorDeclarado numeric(18,2),
@FechaRegistracion datetime,
@IdAutorizaAnulacion int,
@IdPuntoVenta int,
@PuntoVenta int,
@Patente varchar(25),
@Chofer varchar(50),
@NumeroDocumento varchar(30),
@OrdenCarga varchar(10),
@OrdenCompra varchar(10),
@COT varchar(20),
@IdEquipo int,
@IdObra int,
@IdListaPrecios int,
@IdDetalleClienteLugarEntrega int,
@HoraSalida varchar(10),
@PesoBruto numeric(18,2),
@Tara numeric(18,2)

AS 

INSERT INTO Remitos
(
 NumeroRemito,
 IdCliente,
 FechaRemito,
 IdCondicionVenta,
 Anulado,
 FechaAnulacion,
 Observaciones,
 ArchivoAdjunto1,
 ArchivoAdjunto2,
 ArchivoAdjunto3,
 ArchivoAdjunto4,
 ArchivoAdjunto5,
 ArchivoAdjunto6,
 ArchivoAdjunto7,
 ArchivoAdjunto8,
 ArchivoAdjunto9,
 ArchivoAdjunto10,
 Destino,
 IdProveedor,
 IdTransportista,
 TotalBultos,
 ValorDeclarado,
 FechaRegistracion,
 IdAutorizaAnulacion,
 IdPuntoVenta,
 PuntoVenta,
 Patente,
 Chofer,
 NumeroDocumento,
 OrdenCarga,
 OrdenCompra,
 COT,
 IdEquipo,
 IdObra,
 IdListaPrecios,
 IdDetalleClienteLugarEntrega,
 HoraSalida,
 PesoBruto,
 Tara
)
VALUES 
(
 @NumeroRemito,
 @IdCliente,
 @FechaRemito,
 @IdCondicionVenta,
 @Anulado,
 @FechaAnulacion,
 @Observaciones,
 @ArchivoAdjunto1,
 @ArchivoAdjunto2,
 @ArchivoAdjunto3,
 @ArchivoAdjunto4,
 @ArchivoAdjunto5,
 @ArchivoAdjunto6,
 @ArchivoAdjunto7,
 @ArchivoAdjunto8,
 @ArchivoAdjunto9,
 @ArchivoAdjunto10,
 @Destino,
 @IdProveedor,
 @IdTransportista,
 @TotalBultos,
 @ValorDeclarado,
 GetDate(),
 @IdAutorizaAnulacion,
 @IdPuntoVenta,
 @PuntoVenta,
 @Patente,
 @Chofer,
 @NumeroDocumento,
 @OrdenCarga,
 @OrdenCompra,
 @COT,
 @IdEquipo,
 @IdObra,
 @IdListaPrecios,
 @IdDetalleClienteLugarEntrega,
 @HoraSalida,
 @PesoBruto,
 @Tara
)

SELECT @IdRemito=@@identity

RETURN(@IdRemito)
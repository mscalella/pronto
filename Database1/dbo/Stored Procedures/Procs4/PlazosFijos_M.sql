﻿CREATE  Procedure [dbo].[PlazosFijos_M]
@IdPlazoFijo int,
@IdBanco int,
@NumeroCertificado1 int,
@NumeroCertificado2 varchar(20),
@DireccionEmisionYPago varchar(50),
@Titulares varchar(50),
@CodigoDeposito int,
@CodigoClase int,
@PlazoEnDias int,
@TasaNominalAnual numeric(18,3),
@Importe numeric(18,2),
@TasaEfectivaMensual numeric(18,3),
@FechaVencimiento datetime,
@ImporteIntereses numeric(18,2),
@Orden varchar(50),
@Detalle varchar(50),
@IdPlazoFijoOrigen int,
@FechaInicioPlazoFijo datetime,
@IdMoneda int,
@CotizacionMonedaAlInicio numeric(18,4),
@CotizacionMonedaAlFinal numeric(18,4),
@Finalizado varchar(2),
@IdCajaOrigen int,
@IdCuentaBancariaOrigen int,
@IdCajaDestino int,
@IdCuentaBancariaDestino int,
@RetencionGanancia numeric(18,2),
@Anulado varchar(2),
@AcreditarInteresesAlFinalizar varchar(2),
@NoExigirRubroContable varchar(2)

AS

UPDATE PlazosFijos
SET
 IdBanco=@IdBanco,
 NumeroCertificado1=@NumeroCertificado1,
 NumeroCertificado2=@NumeroCertificado2,
 DireccionEmisionYPago=@DireccionEmisionYPago,
 Titulares=@Titulares,
 CodigoDeposito=@CodigoDeposito,
 CodigoClase=@CodigoClase,
 PlazoEnDias=@PlazoEnDias,
 TasaNominalAnual=@TasaNominalAnual,
 Importe=@Importe,
 TasaEfectivaMensual=@TasaEfectivaMensual,
 FechaVencimiento=@FechaVencimiento,
 ImporteIntereses=@ImporteIntereses,
 Orden=@Orden,
 Detalle=@Detalle,
 IdPlazoFijoOrigen=@IdPlazoFijoOrigen,
 FechaInicioPlazoFijo=@FechaInicioPlazoFijo,
 IdMoneda=@IdMoneda,
 CotizacionMonedaAlInicio=@CotizacionMonedaAlInicio,
 CotizacionMonedaAlFinal=@CotizacionMonedaAlFinal,
 Finalizado=@Finalizado,
 IdCajaOrigen=@IdCajaOrigen,
 IdCuentaBancariaOrigen=@IdCuentaBancariaOrigen,
 IdCajaDestino=@IdCajaDestino,
 IdCuentaBancariaDestino=@IdCuentaBancariaDestino,
 RetencionGanancia=@RetencionGanancia,
 Anulado=@Anulado,
 AcreditarInteresesAlFinalizar=@AcreditarInteresesAlFinalizar,
 NoExigirRubroContable=@NoExigirRubroContable
WHERE (IdPlazoFijo=@IdPlazoFijo)

RETURN(@IdPlazoFijo)

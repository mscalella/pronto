﻿
CREATE Procedure [dbo].[wProveedores_A]

@IdProveedor int output,
@RazonSocial varchar(50),
@Direccion varchar(50),
@IdLocalidad smallint,
@CodigoPostal varchar(30),
@IdProvincia tinyint,
@IdPais	int,
@Telefono1 varchar(50),
@Telefono2 varchar(50),
@Fax varchar(50),
@Email varchar(50),
@Cuit varchar(13),
@IdCodigoIva tinyint,
@FechaAlta datetime,
@FechaUltimaCompra datetime,
@Excencion numeric(6,2),
@IdCondicionCompra int,
@Contacto varchar(50),
@IdActividad int,
@Nif varchar(30),
@IdEstado int,
@EstadoFecha datetime,
@EstadoUsuario varchar(20),
@AltaUsuario varchar(20),
@CodigoEmpresa varchar(20),
@Nombre1 varchar(100),
@Nombre2 varchar(100),
@NombreFantasia	varchar(50),
@IGCondicion int,
@IGCertificadoAutoretencion varchar(2),
@IGCertificadoNORetencion varchar(2),
@IGFechaCaducidadExencion datetime,
@IGPorcentajeNORetencion Numeric(6,2),
@IvaAgenteRetencion varchar(2),
@IvaExencionRetencion varchar(2),
@IvaFechaCaducidadExencion datetime,
@IvaPorcentajeExencion Numeric(6,2),
@IBNumeroInscripcion varchar(20),
@IBCondicion int,
@IBFechaCaducidadExencion datetime,
@IBPorcentajeExencion Numeric(6,2),
@SSFechaCaducidadExencion datetime,
@SSPorcentajeExcencion Numeric(6,2),
@PaginaWeb varchar(50),
@Habitual varchar(2),
@Observaciones ntext,
@Saldo numeric(18,2),
@CodigoProveedor int,
@IdCuenta int,
@IdMoneda int,
@LimiteCredito numeric(18,2),
@TipoProveedor int,
@Eventual varchar(2),
@IdTipoRetencionGanancia int,
@Confirmado varchar(2),
@CodigoPresto varchar(13),
@BienesOServicios varchar(1),
@IdIBCondicionPorDefecto int,
@RetenerSUSS varchar(2),
@ChequesALaOrdenDe varchar(50),
@FechaLimiteExentoGanancias datetime,
@FechaLimiteExentoIIBB datetime,
@IdImpuestoDirectoSUSS int,
@Importaciones_NumeroInscripcion varchar(20),
@Importaciones_DenominacionInscripcion varchar(10),
@EnviarEmail tinyint,
@InformacionAuxiliar varchar(50),
@CoeficienteIIBBUnificado numeric(6,2),
@FechaUltimaPresentacionDocumentacion datetime,
@ObservacionesPresentacionDocumentacion ntext,
@FechaLimiteCondicionIVA datetime,
@CodigoSituacionRetencionIVA varchar(1),
@SUSSFechaCaducidadExencion datetime,
@Calificacion int,
@IdUsuarioIngreso int,
@FechaIngreso datetime,
@IdUsuarioModifico int,
@FechaModifico datetime,
@Exterior varchar(2),
@SujetoEmbargado varchar(2),
@SaldoEmbargo numeric(18,2),
@DetalleEmbargo varchar(50),
@PorcentajeIBDirecto numeric(6,2),
@FechaInicioVigenciaIBDirecto datetime,
@FechaFinVigenciaIBDirecto datetime,
@GrupoIIBB int

AS

IF IsNull(@IdProveedor,0)<=0
    BEGIN
	INSERT INTO Proveedores
	(
	 RazonSocial,
	 Direccion,
	 IdLocalidad,
	 CodigoPostal,
	 IdProvincia,
	 IdPais,
	 Telefono1,
	 Telefono2,
	 Fax,
	 Email,
	 Cuit,
	 IdCodigoIva,
	 FechaAlta,
 	 FechaUltimaCompra,
	 Excencion,
	 IdCondicionCompra,
	 Contacto,
	 IdActividad,
	 Nif,
	 IdEstado,
	 EstadoFecha,
	 EstadoUsuario,
	 AltaUsuario,
	 CodigoEmpresa,
	 Nombre1,
	 Nombre2,
	 NombreFantasia,
	 IGCondicion,
	 IGCertificadoAutoretencion,
	 IGCertificadoNORetencion,
	 IGFechaCaducidadExencion,
	 IGPorcentajeNORetencion,
	 IvaAgenteRetencion,
	 IvaExencionRetencion,
	 IvaFechaCaducidadExencion,
	 IvaPorcentajeExencion,
	 IBNumeroInscripcion,
	 IBCondicion,
	 IBFechaCaducidadExencion,
	 IBPorcentajeExencion,
	 SSFechaCaducidadExencion,
	 SSPorcentajeExcencion,
	 PaginaWeb,
	 Habitual,
	 Observaciones,
	 Saldo,
	 CodigoProveedor,
	 IdCuenta,
	 IdMoneda,
	 LimiteCredito,
	 TipoProveedor,
	 Eventual,
	 IdTipoRetencionGanancia,
	 Confirmado,
	 CodigoPresto,
	 BienesOServicios,
	 IdIBCondicionPorDefecto,
	 RetenerSUSS,
	 ChequesALaOrdenDe,
	 FechaLimiteExentoGanancias,
	 FechaLimiteExentoIIBB,
	 IdImpuestoDirectoSUSS,
	 Importaciones_NumeroInscripcion,
	 Importaciones_DenominacionInscripcion,
	 EnviarEmail,
	 InformacionAuxiliar,
	 CoeficienteIIBBUnificado,
	 FechaUltimaPresentacionDocumentacion,
	 ObservacionesPresentacionDocumentacion,
	 FechaLimiteCondicionIVA,
	 CodigoSituacionRetencionIVA,
	 SUSSFechaCaducidadExencion,
	 Calificacion,
	 IdUsuarioIngreso,
	 FechaIngreso,
	 IdUsuarioModifico,
	 FechaModifico,
	 Exterior,
	 SujetoEmbargado,
	 SaldoEmbargo,
	 DetalleEmbargo,
	 PorcentajeIBDirecto,
	 FechaInicioVigenciaIBDirecto,
	 FechaFinVigenciaIBDirecto,
	 GrupoIIBB
	)
	VALUES
	(
	@RazonSocial,
	@Direccion,
	@IdLocalidad,
	@CodigoPostal,
	@IdProvincia,
	@IdPais,
	@Telefono1,
	@Telefono2,
	@Fax,
	@Email,
	@Cuit,
	@IdCodigoIva,
	@FechaAlta,
	@FechaUltimaCompra,
	@Excencion,
	@IdCondicionCompra,
	@Contacto,
	@IdActividad,
	@Nif,
	@IdEstado,
	@EstadoFecha,
	@EstadoUsuario,
	@AltaUsuario,
	@CodigoEmpresa,
	@Nombre1,
	@Nombre2,
	@NombreFantasia,
	@IGCondicion,
	@IGCertificadoAutoretencion,
	@IGCertificadoNORetencion,
	@IGFechaCaducidadExencion,
	@IGPorcentajeNORetencion,
	@IvaAgenteRetencion,
	@IvaExencionRetencion,
	@IvaFechaCaducidadExencion,
	@IvaPorcentajeExencion,
	@IBNumeroInscripcion,
	@IBCondicion,
	@IBFechaCaducidadExencion,
	@IBPorcentajeExencion,
	@SSFechaCaducidadExencion,
	@SSPorcentajeExcencion,
	@PaginaWeb,
	@Habitual,
	@Observaciones,
	@Saldo,
	@CodigoProveedor,
	@IdCuenta,
	@IdMoneda,
	@LimiteCredito,
	@TipoProveedor,
	@Eventual,
	@IdTipoRetencionGanancia,
	@Confirmado,	@CodigoPresto,
	@BienesOServicios,
	@IdIBCondicionPorDefecto,
	@RetenerSUSS,
	@ChequesALaOrdenDe,
	@FechaLimiteExentoGanancias,
	@FechaLimiteExentoIIBB,
	@IdImpuestoDirectoSUSS,
	@Importaciones_NumeroInscripcion,
	@Importaciones_DenominacionInscripcion,
	@EnviarEmail,
	@InformacionAuxiliar,
	@CoeficienteIIBBUnificado,
	@FechaUltimaPresentacionDocumentacion,
	@ObservacionesPresentacionDocumentacion,
	@FechaLimiteCondicionIVA,
	@CodigoSituacionRetencionIVA,
	@SUSSFechaCaducidadExencion,
	@Calificacion,
	@IdUsuarioIngreso,
	@FechaIngreso,
	@IdUsuarioModifico,
	@FechaModifico,
	@Exterior,
	@SujetoEmbargado,
	@SaldoEmbargo,
	@DetalleEmbargo,
	@PorcentajeIBDirecto,
	@FechaInicioVigenciaIBDirecto,
	@FechaFinVigenciaIBDirecto,
	@GrupoIIBB
	)
	SELECT @IdProveedor=@@identity
    END
ELSE
    BEGIN
	UPDATE Proveedores
	SET 
	 RazonSocial=@RazonSocial,
	 Direccion=@Direccion,
	 IdLocalidad=@IdLocalidad,
	 CodigoPostal=@CodigoPostal,
	 IdProvincia=@IdProvincia,
	 IdPais=@IdPais,
	 Telefono1=@Telefono1,
	 Telefono2=@Telefono2,
	 Fax=@Fax,
	 Email=@Email,
	 Cuit=@Cuit,
	 IdCodigoIva=@IdCodigoIva,
	 FechaAlta=@FechaAlta,
	 FechaUltimaCompra=@FechaUltimaCompra,
	 Excencion=@Excencion,
	 IdCondicionCompra=@IdCondicionCompra,
	 Contacto=@Contacto,
	 IdActividad=@IdActividad,
	 Nif=@Nif,
	 IdEstado=@IdEstado,
	 EstadoFecha=@EstadoFecha,
	 EstadoUsuario=@EstadoUsuario,
	 AltaUsuario=@AltaUsuario,
	 CodigoEmpresa=@CodigoEmpresa,
	 Nombre1=@Nombre1,
	 Nombre2=@Nombre2,
	 NombreFantasia=@NombreFantasia,
	 IGCondicion=@IGCondicion,
	 IGCertificadoAutoretencion=@IGCertificadoAutoretencion,
	 IGCertificadoNORetencion=@IGCertificadoNORetencion,
	 IGFechaCaducidadExencion=@IGFechaCaducidadExencion,
	 IGPorcentajeNORetencion=@IGPorcentajeNORetencion,
	 IvaAgenteRetencion=@IvaAgenteRetencion,
	 IvaExencionRetencion=@IvaExencionRetencion,
	 IvaFechaCaducidadExencion=@IvaFechaCaducidadExencion,
	 IvaPorcentajeExencion=@IvaPorcentajeExencion,
	 IBNumeroInscripcion=@IBNumeroInscripcion,
	 IBCondicion=@IBCondicion,
	 IBFechaCaducidadExencion=@IBFechaCaducidadExencion,
	 IBPorcentajeExencion=@IBPorcentajeExencion,
	 SSFechaCaducidadExencion=@SSFechaCaducidadExencion,
	 SSPorcentajeExcencion=@SSPorcentajeExcencion,
	 PaginaWeb=@PaginaWeb,
	 Habitual=@Habitual,
	 Observaciones=@Observaciones,
	 Saldo=@Saldo,
	 CodigoProveedor=@CodigoProveedor,
	 IdCuenta=@IdCuenta,
	 IdMoneda=@IdMoneda,
	 LimiteCredito=@LimiteCredito,
	 TipoProveedor=@TipoProveedor,
	 Eventual=@Eventual,
	 IdTipoRetencionGanancia=@IdTipoRetencionGanancia,
	 Confirmado=@Confirmado,
	 CodigoPresto=@CodigoPresto,
	 BienesOServicios=@BienesOServicios,
	 IdIBCondicionPorDefecto=@IdIBCondicionPorDefecto,	 RetenerSUSS=@RetenerSUSS,
	 ChequesALaOrdenDe=@ChequesALaOrdenDe,
	 FechaLimiteExentoGanancias=@FechaLimiteExentoGanancias,
	 FechaLimiteExentoIIBB=@FechaLimiteExentoIIBB,
	 IdImpuestoDirectoSUSS=@IdImpuestoDirectoSUSS,
	 Importaciones_NumeroInscripcion=@Importaciones_NumeroInscripcion,
	 Importaciones_DenominacionInscripcion=@Importaciones_DenominacionInscripcion,
	 EnviarEmail=@EnviarEmail,
	 InformacionAuxiliar=@InformacionAuxiliar,
	 CoeficienteIIBBUnificado=@CoeficienteIIBBUnificado,
	 FechaUltimaPresentacionDocumentacion=@FechaUltimaPresentacionDocumentacion,
	 ObservacionesPresentacionDocumentacion=@ObservacionesPresentacionDocumentacion,
	 FechaLimiteCondicionIVA=@FechaLimiteCondicionIVA,
	 CodigoSituacionRetencionIVA=@CodigoSituacionRetencionIVA,
	 SUSSFechaCaducidadExencion=@SUSSFechaCaducidadExencion,
	 Calificacion=@Calificacion,
	 IdUsuarioIngreso=@IdUsuarioIngreso,
	 FechaIngreso=@FechaIngreso, 
	 IdUsuarioModifico=@IdUsuarioModifico,
	 FechaModifico=@FechaModifico,
	 Exterior=@Exterior,
	 SujetoEmbargado=@SujetoEmbargado,
	 SaldoEmbargo=@SaldoEmbargo,
	 DetalleEmbargo=@DetalleEmbargo,
	 PorcentajeIBDirecto=@PorcentajeIBDirecto,
	 FechaInicioVigenciaIBDirecto=@FechaInicioVigenciaIBDirecto,
	 FechaFinVigenciaIBDirecto=@FechaFinVigenciaIBDirecto,
	 GrupoIIBB=@GrupoIIBB
	WHERE (IdProveedor=@IdProveedor)
    END

IF @@ERROR <> 0
	RETURN -1
ELSE
	RETURN @IdProveedor


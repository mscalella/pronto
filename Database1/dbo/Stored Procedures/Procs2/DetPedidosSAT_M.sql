﻿
CREATE  Procedure [dbo].[DetPedidosSAT_M]
@IdDetallePedido int output,
@IdPedido int,
@NumeroItem int,
@Cantidad numeric(12,2),
@IdUnidad int,
@IdArticulo int,
@FechaEntrega datetime,
@IdControlCalidad int,
@Precio numeric(12,4),
@Observaciones ntext,
@Cantidad1 numeric(12,2),
@Cantidad2 numeric(12,2),
@IdDetalleAcopios int,
@IdDetalleRequerimiento int,
@Cumplido varchar(2),
@CantidadAdicional numeric(16,2),
@CantidadRecibida numeric(12,2),
@CantidadAdicionalRecibida numeric(16,2),
@Adjunto varchar(2),
@ArchivoAdjunto varchar(50),
@FechaNecesidad datetime,
@IdDetalleLMateriales int,
@IdCuenta int,
@OrigenDescripcion int,
@ArchivoAdjunto1 varchar(100),
@ArchivoAdjunto2 varchar(100),
@ArchivoAdjunto3 varchar(100),
@ArchivoAdjunto4 varchar(100),
@ArchivoAdjunto5 varchar(100),
@ArchivoAdjunto6 varchar(100),
@ArchivoAdjunto7 varchar(100),
@ArchivoAdjunto8 varchar(100),
@ArchivoAdjunto9 varchar(100),
@ArchivoAdjunto10 varchar(100),
@IdAutorizoCumplido int,
@IdDioPorCumplido int,
@FechaDadoPorCumplido datetime,
@ObservacionesCumplido ntext,
@IdCentroCosto int,
@PorcentajeBonificacion numeric(6,2),
@ImporteBonificacion numeric(18,4),
@PorcentajeIVA numeric(6,2),
@ImporteIVA numeric(18,4),
@ImporteTotalItem numeric(18,4),
@Costo numeric(18,2),
@PRESTOPedido varchar(13),
@PRESTOFechaProceso datetime,
@IdAsignacionCosto int,
@CostoAsignado numeric(18,4),
@IdUsuarioAsignoCosto int,
@FechaAsignacionCosto datetime,
@EnviarEmail tinyint,
@IdDetallePedidoOriginal int,
@IdPedidoOriginal int,
@IdOrigenTransmision int

AS

UPDATE DetallePedidosSAT
SET
 IdPedido=@IdPedido,
 NumeroItem=@NumeroItem,
 Cantidad=@Cantidad,
 IdUnidad=@IdUnidad,
 IdArticulo=@IdArticulo,
 FechaEntrega=@FechaEntrega,
 IdControlCalidad=@IdControlCalidad,
 Precio=@Precio,
 Observaciones=@Observaciones,
 Cantidad1=@Cantidad1,
 Cantidad2=@Cantidad2,
 IdDetalleAcopios=@IdDetalleAcopios,
 IdDetalleRequerimiento=@IdDetalleRequerimiento,
 Cumplido=@Cumplido,
 CantidadAdicional=@CantidadAdicional,
 CantidadRecibida=@CantidadRecibida,
 CantidadAdicionalRecibida=@CantidadAdicionalRecibida,
 Adjunto=@Adjunto,
 ArchivoAdjunto=@ArchivoAdjunto,
 FechaNecesidad=@FechaNecesidad,
 IdDetalleLMateriales=@IdDetalleLMateriales,
 IdCuenta=@IdCuenta,
 OrigenDescripcion=@OrigenDescripcion,
 ArchivoAdjunto1=@ArchivoAdjunto1,
 ArchivoAdjunto2=@ArchivoAdjunto2,
 ArchivoAdjunto3=@ArchivoAdjunto3,
 ArchivoAdjunto4=@ArchivoAdjunto4,
 ArchivoAdjunto5=@ArchivoAdjunto5,
 ArchivoAdjunto6=@ArchivoAdjunto6,
 ArchivoAdjunto7=@ArchivoAdjunto7,
 ArchivoAdjunto8=@ArchivoAdjunto8,
 ArchivoAdjunto9=@ArchivoAdjunto9,
 ArchivoAdjunto10=@ArchivoAdjunto10,
 IdAutorizoCumplido=@IdAutorizoCumplido,
 IdDioPorCumplido=@IdDioPorCumplido,
 FechaDadoPorCumplido=@FechaDadoPorCumplido,
 ObservacionesCumplido=@ObservacionesCumplido,
 IdCentroCosto=@IdCentroCosto,
 PorcentajeBonificacion=@PorcentajeBonificacion,
 ImporteBonificacion=@ImporteBonificacion,
 PorcentajeIVA=@PorcentajeIVA,
 ImporteIVA=@ImporteIVA,
 ImporteTotalItem=@ImporteTotalItem,
 Costo=@Costo,
 PRESTOPedido=@PRESTOPedido,
 PRESTOFechaProceso=@PRESTOFechaProceso,
 IdAsignacionCosto=@IdAsignacionCosto,
 CostoAsignado=@CostoAsignado,
 IdUsuarioAsignoCosto=@IdUsuarioAsignoCosto,
 FechaAsignacionCosto=@FechaAsignacionCosto,
 EnviarEmail=@EnviarEmail,
 IdDetallePedidoOriginal=@IdDetallePedidoOriginal,
 IdPedidoOriginal=@IdPedidoOriginal,
 IdOrigenTransmision=@IdOrigenTransmision
WHERE IdDetallePedido=@IdDetallePedido
RETURN(@IdDetallePedido)

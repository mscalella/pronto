﻿
CREATE Procedure [dbo].[DetSalidasMaterialesSAT_M]
@IdDetalleSalidaMateriales int,
@IdSalidaMateriales int,
@IdArticulo int,
@IdStock int,
@Partida int,
@Cantidad numeric(12,2),
@CantidadAdicional numeric(12,2),
@IdUnidad int,
@Cantidad1 numeric(12,2),
@Cantidad2 numeric(12,2),
@IdDetalleValeSalida int,
@Adjunto varchar(2),
@ArchivoAdjunto1 varchar(100),
@ArchivoAdjunto2 varchar(100),
@ArchivoAdjunto3 varchar(100),
@ArchivoAdjunto4 varchar(100),
@ArchivoAdjunto5 varchar(100),
@ArchivoAdjunto6 varchar(100),
@ArchivoAdjunto7 varchar(100),
@ArchivoAdjunto8 varchar(100),
@ArchivoAdjunto9 varchar(100),
@ArchivoAdjunto10 varchar(100),
@Observaciones ntext,
@IdUbicacion int,
@IdObra int,
@CostoUnitario numeric(18,4),
@IdMoneda int,
@CotizacionDolar numeric(18,4),
@CotizacionMoneda numeric(18,4),
@IdEquipoDestino int,
@EnviarEmail tinyint,
@IdDetalleSalidaMaterialesOriginal int,
@IdSalidaMaterialesOriginal int,
@IdOrigenTransmision int
AS 
UPDATE DetalleSalidasMaterialesSAT
SET 
 IdSalidaMateriales=@IdSalidaMateriales,
 IdArticulo=@IdArticulo,
 IdStock=@IdStock,
 Partida=@Partida,
 Cantidad=@Cantidad,
 CantidadAdicional=@CantidadAdicional,
 IdUnidad=@IdUnidad,
 Cantidad1=@Cantidad1,
 Cantidad2=@Cantidad2,
 IdDetalleValeSalida=@IdDetalleValeSalida,
 Adjunto=@Adjunto,
 ArchivoAdjunto1=@ArchivoAdjunto1,
 ArchivoAdjunto2=@ArchivoAdjunto2,
 ArchivoAdjunto3=@ArchivoAdjunto3,
 ArchivoAdjunto4=@ArchivoAdjunto4,
 ArchivoAdjunto5=@ArchivoAdjunto5,
 ArchivoAdjunto6=@ArchivoAdjunto6,
 ArchivoAdjunto7=@ArchivoAdjunto7,
 ArchivoAdjunto8=@ArchivoAdjunto8,
 ArchivoAdjunto9=@ArchivoAdjunto9,
 ArchivoAdjunto10=@ArchivoAdjunto10,
 Observaciones=@Observaciones,
 IdUbicacion=@IdUbicacion,
 IdObra=@IdObra,
 CostoUnitario=@CostoUnitario,
 IdMoneda=@IdMoneda,
 CotizacionDolar=@CotizacionDolar,
 CotizacionMoneda=@CotizacionMoneda,
 IdEquipoDestino=@IdEquipoDestino,
 EnviarEmail=@EnviarEmail,
 IdDetalleSalidaMaterialesOriginal=@IdDetalleSalidaMaterialesOriginal,
 IdSalidaMaterialesOriginal=@IdSalidaMaterialesOriginal,
 IdOrigenTransmision=@IdOrigenTransmision
WHERE (IdDetalleSalidaMateriales=@IdDetalleSalidaMateriales)
RETURN(@IdDetalleSalidaMateriales)

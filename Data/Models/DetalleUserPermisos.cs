//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetalleUserPermisos
    {
        public int IdDetalleUserPermisos { get; set; }
        public Nullable<System.Guid> UserId { get; set; }
        public string Modulo { get; set; }
        public Nullable<bool> PuedeLeer { get; set; }
        public Nullable<bool> PuedeModificar { get; set; }
        public Nullable<bool> PuedeEliminar { get; set; }
        public Nullable<bool> Instalado { get; set; }
    }
}

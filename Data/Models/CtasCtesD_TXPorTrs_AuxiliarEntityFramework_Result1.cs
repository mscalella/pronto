//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    
    public partial class CtasCtesD_TXPorTrs_AuxiliarEntityFramework_Result1
    {
        public Nullable<int> IdCtaCte { get; set; }
        public Nullable<int> IdImputacion { get; set; }
        public string Comp { get; set; }
        public Nullable<int> IdTipoComp { get; set; }
        public Nullable<int> IdComprobante { get; set; }
        public string Numero { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public Nullable<System.DateTime> Fechavt { get; set; }
        public Nullable<decimal> Imporig { get; set; }
        public Nullable<decimal> SaldoComp { get; set; }
        public Nullable<decimal> SaldoTrs { get; set; }
        public string Observaciones { get; set; }
        public string Cabeza { get; set; }
        public Nullable<int> IdImpu { get; set; }
        public Nullable<int> IdAux1 { get; set; }
        public string Condventa { get; set; }
        public string Obra { get; set; }
        public string Ordendecompra { get; set; }
        public string Monorigen { get; set; }
        public string Vendedor { get; set; }
        public string Origen { get; set; }
        public string Vector_E { get; set; }
        public string Vector_T { get; set; }
        public string Vector_X { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Subrubro
    {
        public Subrubro()
        {
            this.Articulos = new HashSet<Articulo>();
        }
    
        public int IdSubrubro { get; set; }
        public string Descripcion { get; set; }
        public string Abreviatura { get; set; }
        public Nullable<byte> EnviarEmail { get; set; }
        public Nullable<int> Codigo { get; set; }
    
        public virtual ICollection<Articulo> Articulos { get; set; }
    }
}

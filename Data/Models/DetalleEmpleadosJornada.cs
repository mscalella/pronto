//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetalleEmpleadosJornada
    {
        public int IdDetalleEmpleadoJornada { get; set; }
        public Nullable<int> IdEmpleado { get; set; }
        public Nullable<System.DateTime> FechaCambio { get; set; }
        public Nullable<decimal> HorasJornada { get; set; }
    
        public virtual Empleado Empleado { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models.Mantenimiento
{
    using System;
    using System.Collections.Generic;
    
    public partial class HorasAseguradasEquipos
    {
        public int IdHorasAseguradasEquipos { get; set; }
        public Nullable<int> IdEquipo { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public Nullable<int> IdObra { get; set; }
        public Nullable<decimal> HorasReales { get; set; }
        public Nullable<decimal> HorasAseguradas { get; set; }
        public Nullable<decimal> HorasAPagar { get; set; }
        public Nullable<decimal> HorasInoperativas { get; set; }
        public Nullable<int> IdDetalleParteDiario { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models.Mantenimiento
{
    using System;
    using System.Collections.Generic;
    
    public partial class HorasEquipo
    {
        public int IdHoraEquipo { get; set; }
        public Nullable<int> IdEquipo { get; set; }
        public Nullable<int> IdObra { get; set; }
        public Nullable<int> IdPresupuestoObrasNodo { get; set; }
        public Nullable<int> IdDetalleObraSector { get; set; }
        public Nullable<int> IdCuadrilla { get; set; }
        public Nullable<System.DateTime> HoraInicial { get; set; }
        public Nullable<System.DateTime> HoraFinal { get; set; }
        public Nullable<int> IdTarea { get; set; }
    }
}

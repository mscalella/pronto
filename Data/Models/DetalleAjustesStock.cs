//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetalleAjustesStock
    {
        public int IdDetalleAjusteStock { get; set; }
        public Nullable<int> IdAjusteStock { get; set; }
        public Nullable<int> IdArticulo { get; set; }
        public string Partida { get; set; }
        public Nullable<decimal> CantidadUnidades { get; set; }
        public Nullable<decimal> CantidadAdicional { get; set; }
        public Nullable<int> IdUnidad { get; set; }
        public Nullable<decimal> Cantidad1 { get; set; }
        public Nullable<decimal> Cantidad2 { get; set; }
        public string Observaciones { get; set; }
        public Nullable<int> IdUbicacion { get; set; }
        public Nullable<int> IdObra { get; set; }
        public Nullable<byte> EnviarEmail { get; set; }
        public Nullable<int> IdDetalleAjusteStockOriginal { get; set; }
        public Nullable<int> IdAjusteStockOriginal { get; set; }
        public Nullable<int> IdOrigenTransmision { get; set; }
        public Nullable<int> IdDetalleSalidaMateriales { get; set; }
        public Nullable<int> NumeroCaja { get; set; }
        public string Talle { get; set; }
        public Nullable<int> IdColor { get; set; }
        public Nullable<decimal> CantidadInventariada { get; set; }
        public Nullable<int> IdDetalleValeSalida { get; set; }
    
        public virtual AjustesStock AjustesStock { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AutorizacionesPorComprobante1
    {
        public int IdAutorizacionPorComprobante { get; set; }
        public Nullable<int> IdFormulario { get; set; }
        public Nullable<int> IdComprobante { get; set; }
        public Nullable<int> OrdenAutorizacion { get; set; }
        public Nullable<int> IdAutorizo { get; set; }
        public Nullable<System.DateTime> FechaAutorizacion { get; set; }
        public string Visto { get; set; }
    }
}

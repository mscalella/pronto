//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class OrdenesTrabajo
    {
        public int IdOrdenTrabajo { get; set; }
        public string NumeroOrdenTrabajo { get; set; }
        public string Descripcion { get; set; }
        public Nullable<System.DateTime> FechaInicio { get; set; }
        public Nullable<System.DateTime> FechaEntrega { get; set; }
        public Nullable<System.DateTime> FechaFinalizacion { get; set; }
        public string TrabajosARealizar { get; set; }
        public Nullable<int> IdOrdeno { get; set; }
        public Nullable<int> IdSuperviso { get; set; }
        public string Observaciones { get; set; }
        public Nullable<int> IdEquipoDestino { get; set; }
    }
}

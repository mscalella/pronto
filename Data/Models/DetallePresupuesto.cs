//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetallePresupuesto
    {
        public int IdDetallePresupuesto { get; set; }
        public Nullable<int> IdPresupuesto { get; set; }
        public Nullable<int> NumeroItem { get; set; }
        public Nullable<int> IdArticulo { get; set; }
        public Nullable<decimal> Cantidad { get; set; }
        public Nullable<int> IdUnidad { get; set; }
        public Nullable<decimal> Precio { get; set; }
        public string Adjunto { get; set; }
        public string ArchivoAdjunto { get; set; }
        public Nullable<decimal> Cantidad1 { get; set; }
        public Nullable<decimal> Cantidad2 { get; set; }
        public string Observaciones { get; set; }
        public Nullable<int> IdDetalleAcopios { get; set; }
        public Nullable<int> IdDetalleRequerimiento { get; set; }
        public Nullable<int> OrigenDescripcion { get; set; }
        public Nullable<int> IdDetalleLMateriales { get; set; }
        public Nullable<int> IdCuenta { get; set; }
        public string ArchivoAdjunto1 { get; set; }
        public string ArchivoAdjunto2 { get; set; }
        public string ArchivoAdjunto3 { get; set; }
        public string ArchivoAdjunto4 { get; set; }
        public string ArchivoAdjunto5 { get; set; }
        public string ArchivoAdjunto6 { get; set; }
        public string ArchivoAdjunto7 { get; set; }
        public string ArchivoAdjunto8 { get; set; }
        public string ArchivoAdjunto9 { get; set; }
        public string ArchivoAdjunto10 { get; set; }
        public Nullable<System.DateTime> FechaEntrega { get; set; }
        public Nullable<int> IdCentroCosto { get; set; }
        public Nullable<decimal> PorcentajeBonificacion { get; set; }
        public Nullable<decimal> ImporteBonificacion { get; set; }
        public Nullable<decimal> PorcentajeIva { get; set; }
        public Nullable<decimal> ImporteIva { get; set; }
        public Nullable<decimal> ImporteTotalItem { get; set; }
    
        public virtual Articulo Articulo { get; set; }
        public virtual Presupuesto Presupuesto { get; set; }
        public virtual Unidad Unidad { get; set; }
        public virtual DetalleRequerimiento DetalleRequerimiento { get; set; }
    }
}

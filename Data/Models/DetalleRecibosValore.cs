//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetalleRecibosValore
    {
        public int IdDetalleReciboValores { get; set; }
        public Nullable<int> IdRecibo { get; set; }
        public Nullable<int> IdTipoValor { get; set; }
        public Nullable<decimal> NumeroValor { get; set; }
        public Nullable<int> NumeroInterno { get; set; }
        public Nullable<System.DateTime> FechaVencimiento { get; set; }
        public Nullable<int> IdBanco { get; set; }
        public Nullable<decimal> Importe { get; set; }
        public Nullable<int> IdCuentaBancariaTransferencia { get; set; }
        public Nullable<int> IdBancoTransferencia { get; set; }
        public Nullable<int> NumeroTransferencia { get; set; }
        public Nullable<int> IdTipoCuentaGrupo { get; set; }
        public Nullable<int> IdCuenta { get; set; }
        public Nullable<int> IdCaja { get; set; }
        public string CuitLibrador { get; set; }
        public Nullable<int> IdTarjetaCredito { get; set; }
        public string NumeroTarjetaCredito { get; set; }
        public Nullable<int> NumeroAutorizacionTarjetaCredito { get; set; }
        public Nullable<int> CantidadCuotas { get; set; }
        public Nullable<byte> EnviarEmail { get; set; }
        public Nullable<int> IdOrigenTransmision { get; set; }
        public Nullable<int> IdReciboOriginal { get; set; }
        public Nullable<int> IdDetalleReciboValoresOriginal { get; set; }
        public Nullable<System.DateTime> FechaImportacionTransmision { get; set; }
        public string FechaExpiracionTarjetaCredito { get; set; }
    
        public virtual Recibo Recibo { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UsuariosRelacionFertilizantesPuntosDespacho
    {
        public int IdUsuariosRelacionFertilizantesPuntosDespacho { get; set; }
        public Nullable<int> IdFertilizantesPuntosDespacho { get; set; }
        public Nullable<System.Guid> UsuarioEnBaseBDLMaster { get; set; }
    
        public virtual FertilizantesPuntosDespacho FertilizantesPuntosDespacho { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProntoMVC.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetalleOrdenesPagoRubrosContable
    {
        public int IdDetalleOrdenPagoRubrosContables { get; set; }
        public Nullable<int> IdOrdenPago { get; set; }
        public Nullable<int> IdRubroContable { get; set; }
        public Nullable<decimal> Importe { get; set; }
    
        public virtual RubrosContable RubrosContable { get; set; }
        public virtual OrdenPago OrdenesPago { get; set; }
    }
}

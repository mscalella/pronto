﻿namespace ProntoMVC.Data.Models
{
    public interface IEntity
    {
        int ID { get; }
    }
}

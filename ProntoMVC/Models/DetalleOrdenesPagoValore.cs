//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(TiposComprobante))]
    public partial class DetalleOrdenesPagoValore
    {
        [DataMember]
        public int IdDetalleOrdenPagoValores { get; set; }
        [DataMember]
        public Nullable<int> IdOrdenPago { get; set; }
        [DataMember]
        public Nullable<int> IdTipoValor { get; set; }
        [DataMember]
        public Nullable<decimal> NumeroValor { get; set; }
        [DataMember]
        public Nullable<int> NumeroInterno { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaVencimiento { get; set; }
        [DataMember]
        public Nullable<int> IdBanco { get; set; }
        [DataMember]
        public Nullable<decimal> Importe { get; set; }
        [DataMember]
        public Nullable<int> IdValor { get; set; }
        [DataMember]
        public Nullable<int> IdCuentaBancaria { get; set; }
        [DataMember]
        public Nullable<int> IdBancoChequera { get; set; }
        [DataMember]
        public Nullable<int> IdCaja { get; set; }
        [DataMember]
        public string ChequesALaOrdenDe { get; set; }
        [DataMember]
        public string NoALaOrden { get; set; }
        [DataMember]
        public string Anulado { get; set; }
        [DataMember]
        public Nullable<int> IdUsuarioAnulo { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaAnulacion { get; set; }
        [DataMember]
        public string MotivoAnulacion { get; set; }
        [DataMember]
        public Nullable<int> IdTarjetaCredito { get; set; }
    
        [DataMember]
        public virtual TiposComprobante TiposComprobante { get; set; }
    }
    
}

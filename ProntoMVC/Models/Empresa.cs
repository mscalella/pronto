//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    public partial class Empresa
    {
        [DataMember]
        public int IdEmpresa { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string DetalleNombre { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public string Localidad { get; set; }
        [DataMember]
        public string CodigoPostal { get; set; }
        [DataMember]
        public string Provincia { get; set; }
        [DataMember]
        public string Telefono1 { get; set; }
        [DataMember]
        public string Telefono2 { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Cuit { get; set; }
        [DataMember]
        public string CondicionIva { get; set; }
        [DataMember]
        public string DatosAdicionales1 { get; set; }
        [DataMember]
        public string DatosAdicionales2 { get; set; }
        [DataMember]
        public string DatosAdicionales3 { get; set; }
        [DataMember]
        public Nullable<int> IdCodigoIva { get; set; }
        [DataMember]
        public string ArchivoAFIP { get; set; }
        [DataMember]
        public Nullable<int> NumeroAgentePercepcionIIBB { get; set; }
        [DataMember]
        public Nullable<int> DigitoVerificadorNumeroAgentePercepcionIIBB { get; set; }
        [DataMember]
        public string ModalidadFacturacionAPrueba { get; set; }
        [DataMember]
        public Nullable<int> CodigoActividadIIBB { get; set; }
        [DataMember]
        public string ActividadComercializacionGranos { get; set; }
        [DataMember]
        public Nullable<int> TipoActividadComercializacionGranos { get; set; }
    }
    
}

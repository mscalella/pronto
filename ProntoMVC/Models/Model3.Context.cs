﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.Metadata.Edm;
using System.Data.Objects.DataClasses;
using System.Data.Objects;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace ProntoMVC.Models
{
    public partial class BDLMasterEntities : ObjectContext
    {
        public const string ConnectionString = "name=BDLMasterEntities";
        public const string ContainerName = "BDLMasterEntities";
    
        #region Constructors
    
        public BDLMasterEntities()
            : base(ConnectionString, ContainerName)
        {
            Initialize();
        }
    
        public BDLMasterEntities(string connectionString)
            : base(connectionString, ContainerName)
        {
            Initialize();
        }
    
        public BDLMasterEntities(EntityConnection connection)
            : base(connection, ContainerName)
        {
            Initialize();
        }
    
        private void Initialize()
        {
            // Creating proxies requires the use of the ProxyDataContractResolver and
            // may allow lazy loading which can expand the loaded graph during serialization.
            ContextOptions.ProxyCreationEnabled = false;
            ObjectMaterialized += new ObjectMaterializedEventHandler(HandleObjectMaterialized);
        }
    
        private void HandleObjectMaterialized(object sender, ObjectMaterializedEventArgs e)
        {
            var entity = e.Entity as IObjectWithChangeTracker;
            if (entity != null)
            {
                bool changeTrackingEnabled = entity.ChangeTracker.ChangeTrackingEnabled;
                try
                {
                    entity.MarkAsUnchanged();
                }
                finally
                {
                    entity.ChangeTracker.ChangeTrackingEnabled = changeTrackingEnabled;
                }
                this.StoreReferenceKeyValues(entity);
            }
        }
    
        #endregion
    
        #region ObjectSet Properties
    
        public ObjectSet<Bases> Bases
        {
            get { return _bases  ?? (_bases = CreateObjectSet<Bases>("Bases")); }
        }
        private ObjectSet<Bases> _bases;
    
        public ObjectSet<DetalleUserBD> DetalleUserBD
        {
            get { return _detalleUserBD  ?? (_detalleUserBD = CreateObjectSet<DetalleUserBD>("DetalleUserBD")); }
        }
        private ObjectSet<DetalleUserBD> _detalleUserBD;
    
        public ObjectSet<DetalleUserPermisos> DetalleUserPermisos
        {
            get { return _detalleUserPermisos  ?? (_detalleUserPermisos = CreateObjectSet<DetalleUserPermisos>("DetalleUserPermisos")); }
        }
        private ObjectSet<DetalleUserPermisos> _detalleUserPermisos;
    
        public ObjectSet<UserDatosExtendidos> UserDatosExtendidos
        {
            get { return _userDatosExtendidos  ?? (_userDatosExtendidos = CreateObjectSet<UserDatosExtendidos>("UserDatosExtendidos")); }
        }
        private ObjectSet<UserDatosExtendidos> _userDatosExtendidos;
    
        public ObjectSet<aspnet_Applications> aspnet_Applications
        {
            get { return _aspnet_Applications  ?? (_aspnet_Applications = CreateObjectSet<aspnet_Applications>("aspnet_Applications")); }
        }
        private ObjectSet<aspnet_Applications> _aspnet_Applications;
    
        public ObjectSet<aspnet_Membership> aspnet_Membership
        {
            get { return _aspnet_Membership  ?? (_aspnet_Membership = CreateObjectSet<aspnet_Membership>("aspnet_Membership")); }
        }
        private ObjectSet<aspnet_Membership> _aspnet_Membership;
    
        public ObjectSet<aspnet_Paths> aspnet_Paths
        {
            get { return _aspnet_Paths  ?? (_aspnet_Paths = CreateObjectSet<aspnet_Paths>("aspnet_Paths")); }
        }
        private ObjectSet<aspnet_Paths> _aspnet_Paths;
    
        public ObjectSet<aspnet_PersonalizationAllUsers> aspnet_PersonalizationAllUsers
        {
            get { return _aspnet_PersonalizationAllUsers  ?? (_aspnet_PersonalizationAllUsers = CreateObjectSet<aspnet_PersonalizationAllUsers>("aspnet_PersonalizationAllUsers")); }
        }
        private ObjectSet<aspnet_PersonalizationAllUsers> _aspnet_PersonalizationAllUsers;
    
        public ObjectSet<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser
        {
            get { return _aspnet_PersonalizationPerUser  ?? (_aspnet_PersonalizationPerUser = CreateObjectSet<aspnet_PersonalizationPerUser>("aspnet_PersonalizationPerUser")); }
        }
        private ObjectSet<aspnet_PersonalizationPerUser> _aspnet_PersonalizationPerUser;
    
        public ObjectSet<aspnet_Profile> aspnet_Profile
        {
            get { return _aspnet_Profile  ?? (_aspnet_Profile = CreateObjectSet<aspnet_Profile>("aspnet_Profile")); }
        }
        private ObjectSet<aspnet_Profile> _aspnet_Profile;
    
        public ObjectSet<aspnet_Roles> aspnet_Roles
        {
            get { return _aspnet_Roles  ?? (_aspnet_Roles = CreateObjectSet<aspnet_Roles>("aspnet_Roles")); }
        }
        private ObjectSet<aspnet_Roles> _aspnet_Roles;
    
        public ObjectSet<aspnet_SchemaVersions> aspnet_SchemaVersions
        {
            get { return _aspnet_SchemaVersions  ?? (_aspnet_SchemaVersions = CreateObjectSet<aspnet_SchemaVersions>("aspnet_SchemaVersions")); }
        }
        private ObjectSet<aspnet_SchemaVersions> _aspnet_SchemaVersions;
    
        public ObjectSet<aspnet_Users> aspnet_Users
        {
            get { return _aspnet_Users  ?? (_aspnet_Users = CreateObjectSet<aspnet_Users>("aspnet_Users")); }
        }
        private ObjectSet<aspnet_Users> _aspnet_Users;
    
        public ObjectSet<aspnet_WebEvent_Events> aspnet_WebEvent_Events
        {
            get { return _aspnet_WebEvent_Events  ?? (_aspnet_WebEvent_Events = CreateObjectSet<aspnet_WebEvent_Events>("aspnet_WebEvent_Events")); }
        }
        private ObjectSet<aspnet_WebEvent_Events> _aspnet_WebEvent_Events;
    
        public ObjectSet<dtproperties> dtproperties
        {
            get { return _dtproperties  ?? (_dtproperties = CreateObjectSet<dtproperties>("dtproperties")); }
        }
        private ObjectSet<dtproperties> _dtproperties;
    
        public ObjectSet<vw_aspnet_UsersInRoles> vw_aspnet_UsersInRoles
        {
            get { return _vw_aspnet_UsersInRoles  ?? (_vw_aspnet_UsersInRoles = CreateObjectSet<vw_aspnet_UsersInRoles>("vw_aspnet_UsersInRoles")); }
        }
        private ObjectSet<vw_aspnet_UsersInRoles> _vw_aspnet_UsersInRoles;

        #endregion
    }
}

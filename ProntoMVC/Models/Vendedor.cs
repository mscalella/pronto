//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Cliente))]
    [KnownType(typeof(Factura))]
    public partial class Vendedor
    {
        public Vendedor()
        {
            this.Clientes = new HashSet<Cliente>();
            this.Clientes1 = new HashSet<Cliente>();
            this.Facturas = new HashSet<Factura>();
        }
    
        [DataMember]
        public int IdVendedor { get; set; }
        [DataMember]
        public int CodigoVendedor { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public short IdLocalidad { get; set; }
        [DataMember]
        public Nullable<int> CodigoPostal { get; set; }
        [DataMember]
        public byte IdProvincia { get; set; }
        [DataMember]
        public string Telefono { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public Nullable<decimal> Comision { get; set; }
        [DataMember]
        public Nullable<int> IdEmpleado { get; set; }
        [DataMember]
        public string Cuit { get; set; }
        [DataMember]
        public string TodasLasZonas { get; set; }
        [DataMember]
        public string EmiteComision { get; set; }
        [DataMember]
        public string IdsVendedoresAsignados { get; set; }
    
        [DataMember]
        public virtual ICollection<Cliente> Clientes { get; set; }
        [DataMember]
        public virtual ICollection<Cliente> Clientes1 { get; set; }
        [DataMember]
        public virtual ICollection<Factura> Facturas { get; set; }
    }
    
}

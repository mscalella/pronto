//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(TiposRetencionGanancia))]
    public partial class Ganancia
    {
        [DataMember]
        public int IdGanancia { get; set; }
        [DataMember]
        public Nullable<decimal> Desde { get; set; }
        [DataMember]
        public Nullable<decimal> Hasta { get; set; }
        [DataMember]
        public Nullable<decimal> SumaFija { get; set; }
        [DataMember]
        public Nullable<decimal> PorcentajeAdicional { get; set; }
        [DataMember]
        public Nullable<int> IdTipoRetencionGanancia { get; set; }
        [DataMember]
        public Nullable<decimal> MinimoNoImponible { get; set; }
        [DataMember]
        public Nullable<decimal> MinimoARetener { get; set; }
    
        [DataMember]
        public virtual TiposRetencionGanancia TiposRetencionGanancia { get; set; }
    }
    
}

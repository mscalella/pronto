//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    public partial class DetalleAutorizacione
    {
        [DataMember]
        public int IdDetalleAutorizacion { get; set; }
        [DataMember]
        public Nullable<int> IdAutorizacion { get; set; }
        [DataMember]
        public Nullable<int> OrdenAutorizacion { get; set; }
        [DataMember]
        public string SectorEmisor1 { get; set; }
        [DataMember]
        public Nullable<int> IdSectorAutoriza1 { get; set; }
        [DataMember]
        public Nullable<int> IdCargoAutoriza1 { get; set; }
        [DataMember]
        public string SectorEmisor2 { get; set; }
        [DataMember]
        public Nullable<int> IdSectorAutoriza2 { get; set; }
        [DataMember]
        public Nullable<int> IdCargoAutoriza2 { get; set; }
        [DataMember]
        public string SectorEmisor3 { get; set; }
        [DataMember]
        public Nullable<int> IdSectorAutoriza3 { get; set; }
        [DataMember]
        public Nullable<int> IdCargoAutoriza3 { get; set; }
        [DataMember]
        public string SectorEmisor4 { get; set; }
        [DataMember]
        public Nullable<int> IdSectorAutoriza4 { get; set; }
        [DataMember]
        public Nullable<int> IdCargoAutoriza4 { get; set; }
        [DataMember]
        public string SectorEmisor5 { get; set; }
        [DataMember]
        public Nullable<int> IdSectorAutoriza5 { get; set; }
        [DataMember]
        public Nullable<int> IdCargoAutoriza5 { get; set; }
        [DataMember]
        public string SectorEmisor6 { get; set; }
        [DataMember]
        public Nullable<int> IdSectorAutoriza6 { get; set; }
        [DataMember]
        public Nullable<int> IdCargoAutoriza6 { get; set; }
        [DataMember]
        public Nullable<int> IdFirmante1 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteDesde1 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteHasta1 { get; set; }
        [DataMember]
        public Nullable<int> IdFirmante2 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteDesde2 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteHasta2 { get; set; }
        [DataMember]
        public Nullable<int> IdFirmante3 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteDesde3 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteHasta3 { get; set; }
        [DataMember]
        public Nullable<int> IdFirmante4 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteDesde4 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteHasta4 { get; set; }
        [DataMember]
        public Nullable<int> IdFirmante5 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteDesde5 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteHasta5 { get; set; }
        [DataMember]
        public Nullable<int> IdFirmante6 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteDesde6 { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteHasta6 { get; set; }
        [DataMember]
        public Nullable<int> PersonalObra1 { get; set; }
        [DataMember]
        public Nullable<int> PersonalObra2 { get; set; }
        [DataMember]
        public Nullable<int> PersonalObra3 { get; set; }
        [DataMember]
        public Nullable<int> PersonalObra4 { get; set; }
        [DataMember]
        public Nullable<int> PersonalObra5 { get; set; }
        [DataMember]
        public Nullable<int> PersonalObra6 { get; set; }
        [DataMember]
        public string ADesignar { get; set; }
        [DataMember]
        public string IdsTipoCompra { get; set; }
    }
    
}

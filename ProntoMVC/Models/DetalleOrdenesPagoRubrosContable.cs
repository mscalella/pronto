//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(RubrosContable))]
    public partial class DetalleOrdenesPagoRubrosContable
    {
        [DataMember]
        public int IdDetalleOrdenPagoRubrosContables { get; set; }
        [DataMember]
        public Nullable<int> IdOrdenPago { get; set; }
        [DataMember]
        public Nullable<int> IdRubroContable { get; set; }
        [DataMember]
        public Nullable<decimal> Importe { get; set; }
    
        [DataMember]
        public virtual RubrosContable RubrosContable { get; set; }
    }
    
}

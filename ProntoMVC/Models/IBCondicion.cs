//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Cliente))]
    [KnownType(typeof(Cuenta))]
    [KnownType(typeof(Provincia))]
    [KnownType(typeof(DetalleProveedoresIB))]
    public partial class IBCondicion
    {
        public IBCondicion()
        {
            this.Clientes = new HashSet<Cliente>();
            this.Clientes1 = new HashSet<Cliente>();
            this.Clientes1_1 = new HashSet<Cliente>();
            this.Clientes2 = new HashSet<Cliente>();
            this.DetalleProveedoresIBs = new HashSet<DetalleProveedoresIB>();
        }
    
        [DataMember]
        public int IdIBCondicion { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteTopeMinimo { get; set; }
        [DataMember]
        public Nullable<decimal> Alicuota { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaVigencia { get; set; }
        [DataMember]
        public string AcumulaMensualmente { get; set; }
        [DataMember]
        public string BaseCalculo { get; set; }
        [DataMember]
        public Nullable<decimal> AlicuotaPercepcion { get; set; }
        [DataMember]
        public Nullable<int> IdProvincia { get; set; }
        [DataMember]
        public Nullable<decimal> ImporteTopeMinimoPercepcion { get; set; }
        [DataMember]
        public Nullable<decimal> AlicuotaPercepcionConvenio { get; set; }
        [DataMember]
        public Nullable<int> IdCuentaPercepcionIIBB { get; set; }
        [DataMember]
        public Nullable<int> IdCuentaPercepcionIIBBConvenio { get; set; }
        [DataMember]
        public Nullable<decimal> PorcentajeATomarSobreBase { get; set; }
        [DataMember]
        public Nullable<decimal> PorcentajeAdicional { get; set; }
        [DataMember]
        public string LeyendaPorcentajeAdicional { get; set; }
        [DataMember]
        public Nullable<int> Codigo { get; set; }
        [DataMember]
        public Nullable<int> CodigoAFIP { get; set; }
        [DataMember]
        public string InformacionAuxiliar { get; set; }
        [DataMember]
        public Nullable<int> IdCuentaPercepcionIIBBCompras { get; set; }
        [DataMember]
        public Nullable<int> IdProvinciaReal { get; set; }
        [DataMember]
        public Nullable<int> CodigoNormaRetencion { get; set; }
        [DataMember]
        public Nullable<int> CodigoNormaPercepcion { get; set; }
        [DataMember]
        public Nullable<int> CodigoActividad { get; set; }
        [DataMember]
        public Nullable<decimal> Alícuota_1 { get; set; }
    
        [DataMember]
        public virtual ICollection<Cliente> Clientes { get; set; }
        [DataMember]
        public virtual ICollection<Cliente> Clientes1 { get; set; }
        [DataMember]
        public virtual Cuenta CuentaIIBBnormal { get; set; }
        [DataMember]
        public virtual Provincia Provincia { get; set; }
        [DataMember]
        public virtual Provincia ProvinciaReal { get; set; }
        [DataMember]
        public virtual ICollection<Cliente> Clientes1_1 { get; set; }
        [DataMember]
        public virtual ICollection<Cliente> Clientes2 { get; set; }
        [DataMember]
        public virtual Cuenta CuentaIIBBcompras { get; set; }
        [DataMember]
        public virtual Cuenta CuentaIIBBconvenio { get; set; }
        [DataMember]
        public virtual ICollection<DetalleProveedoresIB> DetalleProveedoresIBs { get; set; }
    }
    
}

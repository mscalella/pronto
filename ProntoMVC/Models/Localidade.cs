//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Cliente))]
    [KnownType(typeof(Proveedor))]
    public partial class Localidade
    {
        public Localidade()
        {
            this.Clientes = new HashSet<Cliente>();
            this.Clientes1 = new HashSet<Cliente>();
            this.Proveedores = new HashSet<Proveedor>();
        }
    
        [DataMember]
        public int IdLocalidad { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string CodigoPostal { get; set; }
        [DataMember]
        public Nullable<int> IdProvincia { get; set; }
        [DataMember]
        public Nullable<byte> EnviarEmail { get; set; }
        [DataMember]
        public string CodigoONCAA__ { get; set; }
        [DataMember]
        public string CodigoWilliams { get; set; }
        [DataMember]
        public string CodigoLosGrobo { get; set; }
    
        [DataMember]
        public virtual ICollection<Cliente> Clientes { get; set; }
        [DataMember]
        public virtual ICollection<Cliente> Clientes1 { get; set; }
        [DataMember]
        public virtual ICollection<Proveedor> Proveedores { get; set; }
    }
    
}

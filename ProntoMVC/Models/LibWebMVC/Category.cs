﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Northwind.Model
{
    public class Category
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}

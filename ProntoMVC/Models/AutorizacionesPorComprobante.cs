//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    public partial class AutorizacionesPorComprobante
    {
        [DataMember]
        public int IdAutorizacionPorComprobante { get; set; }
        [DataMember]
        public Nullable<int> IdFormulario { get; set; }
        [DataMember]
        public Nullable<int> IdComprobante { get; set; }
        [DataMember]
        public Nullable<int> OrdenAutorizacion { get; set; }
        [DataMember]
        public Nullable<int> IdAutorizo { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaAutorizacion { get; set; }
        [DataMember]
        public string Visto { get; set; }
        [DataMember]
        public string Iniciales { get; set; }
        [DataMember]
        public string Sector { get; set; }
        [DataMember]
        public string Nombre { get; set; }
    }
    
}

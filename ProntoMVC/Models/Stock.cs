//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    public partial class Stock
    {
        [DataMember]
        public int IdStock { get; set; }
        [DataMember]
        public Nullable<int> IdArticulo { get; set; }
        [DataMember]
        public string Partida { get; set; }
        [DataMember]
        public Nullable<decimal> CantidadUnidades { get; set; }
        [DataMember]
        public Nullable<decimal> CantidadAdicional { get; set; }
        [DataMember]
        public Nullable<int> IdUnidad { get; set; }
        [DataMember]
        public Nullable<int> IdUbicacion { get; set; }
        [DataMember]
        public Nullable<int> IdObra { get; set; }
        [DataMember]
        public Nullable<int> NumeroCaja { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaAlta { get; set; }
        [DataMember]
        public Nullable<decimal> Equivalencia { get; set; }
        [DataMember]
        public Nullable<decimal> CantidadEquivalencia { get; set; }
        [DataMember]
        public Nullable<int> IdColor { get; set; }
        [DataMember]
        public string Talle { get; set; }
    }
    
}

//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ProntoMVC.Models
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(DetallePedido))]
    [KnownType(typeof(Empleado))]
    [KnownType(typeof(Moneda))]
    [KnownType(typeof(Proveedor))]
    public partial class Pedido
    {
        public Pedido()
        {
            this.DetallePedidos = new HashSet<DetallePedido>();
        }
    
        [DataMember]
        public int IdPedido { get; set; }
        [DataMember]
        public Nullable<int> NumeroPedido { get; set; }
        [DataMember]
        public Nullable<int> IdProveedor { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaPedido { get; set; }
        [DataMember]
        public string LugarEntrega { get; set; }
        [DataMember]
        public string FormaPago { get; set; }
        [DataMember]
        public string Observaciones { get; set; }
        [DataMember]
        public Nullable<decimal> Bonificacion { get; set; }
        [DataMember]
        public Nullable<decimal> TotalIva1 { get; set; }
        [DataMember]
        public Nullable<decimal> TotalIva2 { get; set; }
        [DataMember]
        public Nullable<decimal> TotalPedido { get; set; }
        [DataMember]
        public Nullable<decimal> PorcentajeIva1 { get; set; }
        [DataMember]
        public Nullable<decimal> PorcentajeIva2 { get; set; }
        [DataMember]
        public Nullable<int> IdComprador { get; set; }
        [DataMember]
        public Nullable<decimal> PorcentajeBonificacion { get; set; }
        [DataMember]
        public Nullable<int> NumeroComparativa { get; set; }
        [DataMember]
        public string Contacto { get; set; }
        [DataMember]
        public string PlazoEntrega { get; set; }
        [DataMember]
        public string Garantia { get; set; }
        [DataMember]
        public string Documentacion { get; set; }
        [DataMember]
        public Nullable<int> Aprobo { get; set; }
        [DataMember]
        public Nullable<int> IdMoneda { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaAprobacion { get; set; }
        [DataMember]
        public string Importante { get; set; }
        [DataMember]
        public Nullable<int> TipoCompra { get; set; }
        [DataMember]
        public string Consorcial { get; set; }
        [DataMember]
        public string Cumplido { get; set; }
        [DataMember]
        public string DetalleCondicionCompra { get; set; }
        [DataMember]
        public Nullable<int> IdAutorizoCumplido { get; set; }
        [DataMember]
        public Nullable<int> IdDioPorCumplido { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaDadoPorCumplido { get; set; }
        [DataMember]
        public string ObservacionesCumplido { get; set; }
        [DataMember]
        public Nullable<int> SubNumero { get; set; }
        [DataMember]
        public string UsuarioAnulacion { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaAnulacion { get; set; }
        [DataMember]
        public string MotivoAnulacion { get; set; }
        [DataMember]
        public string ArchivoAdjunto1 { get; set; }
        [DataMember]
        public string ArchivoAdjunto2 { get; set; }
        [DataMember]
        public string ArchivoAdjunto3 { get; set; }
        [DataMember]
        public string ArchivoAdjunto4 { get; set; }
        [DataMember]
        public string ArchivoAdjunto5 { get; set; }
        [DataMember]
        public string ArchivoAdjunto6 { get; set; }
        [DataMember]
        public string ArchivoAdjunto7 { get; set; }
        [DataMember]
        public string ArchivoAdjunto8 { get; set; }
        [DataMember]
        public string ArchivoAdjunto9 { get; set; }
        [DataMember]
        public string ArchivoAdjunto10 { get; set; }
        [DataMember]
        public string ImprimeImportante { get; set; }
        [DataMember]
        public string ImprimePlazoEntrega { get; set; }
        [DataMember]
        public string ImprimeLugarEntrega { get; set; }
        [DataMember]
        public string ImprimeFormaPago { get; set; }
        [DataMember]
        public string ImprimeImputaciones { get; set; }
        [DataMember]
        public string ImprimeInspecciones { get; set; }
        [DataMember]
        public string ImprimeGarantia { get; set; }
        [DataMember]
        public string ImprimeDocumentacion { get; set; }
        [DataMember]
        public Nullable<decimal> CotizacionDolar { get; set; }
        [DataMember]
        public Nullable<decimal> CotizacionMoneda { get; set; }
        [DataMember]
        public string PedidoExterior { get; set; }
        [DataMember]
        public string PRESTOPedido { get; set; }
        [DataMember]
        public Nullable<System.DateTime> PRESTOFechaProceso { get; set; }
        [DataMember]
        public Nullable<int> IdCondicionCompra { get; set; }
        [DataMember]
        public Nullable<byte> EnviarEmail { get; set; }
        [DataMember]
        public Nullable<int> IdPedidoOriginal { get; set; }
        [DataMember]
        public Nullable<int> IdOrigenTransmision { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaImportacionTransmision { get; set; }
        [DataMember]
        public string Subcontrato { get; set; }
        [DataMember]
        public Nullable<int> IdPedidoAbierto { get; set; }
        [DataMember]
        public string NumeroLicitacion { get; set; }
        [DataMember]
        public string Transmitir_a_SAT { get; set; }
        [DataMember]
        public string NumeracionAutomatica { get; set; }
        [DataMember]
        public string Impresa { get; set; }
        [DataMember]
        public string EmbarcadoA { get; set; }
        [DataMember]
        public string FacturarA { get; set; }
        [DataMember]
        public string ProveedorExt { get; set; }
        [DataMember]
        public Nullable<decimal> ImpuestosInternos { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaSalida { get; set; }
        [DataMember]
        public Nullable<decimal> CodigoControl { get; set; }
        [DataMember]
        public string CircuitoFirmasCompleto { get; set; }
        [DataMember]
        public Nullable<decimal> OtrosConceptos1 { get; set; }
        [DataMember]
        public Nullable<decimal> OtrosConceptos2 { get; set; }
        [DataMember]
        public Nullable<decimal> OtrosConceptos3 { get; set; }
        [DataMember]
        public Nullable<decimal> OtrosConceptos4 { get; set; }
        [DataMember]
        public Nullable<decimal> OtrosConceptos5 { get; set; }
        [DataMember]
        public Nullable<int> IdClausula { get; set; }
        [DataMember]
        public string IncluirObservacionesRM { get; set; }
        [DataMember]
        public Nullable<int> NumeroSubcontrato { get; set; }
        [DataMember]
        public Nullable<int> IdPuntoVenta { get; set; }
        [DataMember]
        public Nullable<int> PuntoVenta { get; set; }
        [DataMember]
        public Nullable<int> IdMonedaOriginal { get; set; }
        [DataMember]
        public Nullable<int> IdLugarEntrega_1 { get; set; }
        [DataMember]
        public string ConfirmadoPorWeb_1 { get; set; }
        [DataMember]
        public Nullable<int> IdTipoCompraRM { get; set; }
    
        [DataMember]
        public virtual ICollection<DetallePedido> DetallePedidos { get; set; }
        [DataMember]
        public virtual Empleado Comprador { get; set; }
        [DataMember]
        public virtual Moneda Moneda { get; set; }
        [DataMember]
        public virtual Proveedor Proveedor { get; set; }
        [DataMember]
        public virtual Empleado Empleado { get; set; }
    }
    
}
